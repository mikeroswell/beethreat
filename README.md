# Overview
This repository contains code and data for predicting which unassessed regional taxa are likely to face conservation threats... using occurrence data, covariates, and a random forest classifiers.

# large files aqnd git lfs
This is the first time I'm using git lfs to handle large files in the repo. I'm not yet sure how the files tracked by lfs are handled if you clone this repo and have not installed git lfs. For more information on git lfs, see [here](https://git-lfs.github.com/) 
